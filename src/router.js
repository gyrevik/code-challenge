const Router = require('koa-router'),
  router = new Router(),
  model = require('./rhinoceros');

router.get('/endangered', (ctx, next) => {
  const rhinoceroses = model.getEndangered();
  ctx.response.body = { rhinoceroses };
});

router.get('/rhinoceros', (ctx, next) => {
  const rhinoceroses = model.getAll();
  ctx.response.body = { rhinoceroses };
});

router.get('/rhinoceros/:id', (ctx, next) => {
  ctx.response.body = model.getById(ctx.params.id);
});

router.get('/rhinocerosbyname/:name', (ctx, next) => {
  ctx.response.body = model.getByName(ctx.params.name);
});

router.post('/rhinoceros', (ctx, next) => {
  const valid = model.validate(ctx.request.body);
  if (valid.errMsg != '') {
    console.log('Validation failed with error message: ', valid.errMsg);
    console.log('No new rhinoceros for you!');
    return;
  }

  ctx.response.body = model.newRhinoceros(ctx.request.body);
  console.log('ctx.response.body: ', ctx.response.body);
});

module.exports = router;
