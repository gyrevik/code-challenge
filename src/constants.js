module.exports = { 
  species: { 
    white_rhinoceros: 'white_rhinoceros', black_rhinoceros: 'black_rhinoceros', 
    indian_rhinoceros: 'indian_rhinoceros', javan_rhinoceros: 'javan_rhinoceros', 
    sumatran_rhinoceros: 'sumatran_rhinoceros'
  },
  keys: { name: 'name', species: 'species' }
};