const uuidv4 = require('uuid/v4');
let rhinoceroses = require('./data');
const constants = require('./constants');

exports.getAll = () => {
  return rhinoceroses;
};

exports.getById = (id) => {
  return rhinoceroses.filter(rhino => rhino.id === id);
};

exports.getByName = (name) => {
  return rhinoceroses.filter(rhino => rhino.name.toUpperCase() === name.toUpperCase());
};

exports.getEndangered = () => {
  populations = {}, endangered = [];
  rhinoceroses.forEach(rhino => {
    if (populations[rhino.species] === undefined) populations[rhino.species] = 0;
    populations[rhino.species] += 1;
  });

  return rhinoceroses.filter(rhino => populations[rhino.species] <= 2 );
};

exports.newRhinoceros = data => {
  const newRhino = {
    id: uuidv4(),
    name: data.name,
    species: data.species
  };
  rhinoceroses.push(newRhino);
  return newRhino;
};

exports.validate = data => {
  const numKeysAllowed = Object.keys(constants.keys).length;
  const numKeys = Object.keys(data).length;

  let errMsg = '';
  Object.keys(data).forEach(key => {
    if (!constants.keys.hasOwnProperty(key)) {
      errMsg = 'Key ' + key + ' is not valid. Please do not post it';
    }
  })

  if (errMsg != '')
    return { errMsg };

  if (numKeys != numKeysAllowed)
    return { errMsg: 'Object has wrong number of keys (' + numKeys + '). The number of keys allowed is ' + numKeysAllowed + '.'};

  if (data.name.length < 1)
    return { errMsg: 'Name too short. Must be at least 1 character long' };

  if (data.name > 20)
    return { errMsg: 'Name too long. Cannot be more then 20 characters' };

  if (!constants.species.hasOwnProperty(data.species))
    return { errMsg: 'Species not valid. Valid species are: ' + Object.keys(constants.species) };
  else
    console.log('species ', data.species, ' is valid');

  return { errMsg };
};